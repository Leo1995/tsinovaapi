<?php

header("Content-type: application/json");

function getFieldTablePotency($text)
{
  if (preg_match('/^.*<FieldLabel1>Potência \\(W\\)<\/FieldLabel1>.*$/', $text, $matches)) {
    return array("rack"=>"PowerLimit", "equipament"=>"PowerUsage");
  }
  if (preg_match('/^.*<FieldLabel2>Potência \\(W\\)<\/FieldLabel2>.*$/', $text, $matches2)) {
    return array("rack"=>"WeightLimit", "equipament"=>"WeightUsage");
  }
  return false;
}



function getFieldTableElectrical($text)
{
  if (preg_match('/^.*<FieldLabel1>Qtd. Tomadas<\/FieldLabel1>.*$/', $text, $matches)) {
    return array("rack"=>"PowerLimit", "equipament"=>"PowerUsage");
  }
  if (preg_match('/^.*<FieldLabel2>Qtd. Tomadas<\/FieldLabel2>.*$/', $text, $matches2)) {
    return array("rack"=>"WeightLimit", "equipament"=>"WeightUsage");
  }
  return false;
}



function getColumnsCapacity($con)
{
  try {
    $sql = "SELECT OptValue as value FROM SMOptionSetting WHERE OptName = 'RackCapacityFields'";
    $result = sqlsrv_query($con, $sql);
    while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {
      return array("potency"=>getFieldTablePotency($row['value']), "electrical"=>getFieldTableElectrical($row['value']));
    }
    return false;
  } catch (Exception $ex) {
    throw $ex;
  } finally {
    if ($con) {
      sqlsrv_free_stmt($result);
    }
  }
}



function getStatus($cableStatus, $patchStatus, $pendingStatus, $reserved, $broken, $critical, $type="patchpanel")
{

  if ($type == "switch"){

    if ($cableStatus == 0 && $patchStatus == 0){
      return 1;
    }

    if ($cableStatus == 0 && $patchStatus == 1){
      return 2;
    }

    return 3;

  }else{

    # Porta disponível
    if ($cableStatus == 1 && $patchStatus == 0 && $pendingStatus == 0 && $reserved == 0 && $broken == 0) {
      return 1;
    }
    # Porta usada
    if ($cableStatus == 1 && ($patchStatus == 1 || $pendingStatus == 1 || $reserved == 1 || $broken == 1)) {
      return 2;
    }
    # Porta indisponível (sem utilidade)
    if ($cableStatus == 0) {
      return 3;
    }

  }

  return 4;
}

?>
