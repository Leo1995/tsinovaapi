<?php

header('Content-Type: application/json');

include "config.php";
include "util.php";

// Pega todas as empresas de uma resposta JSON
function getCompanies($companies_json)
{
  $listCompanies = array();
  foreach ($companies_json as $key => $value) {
    if ($value['id_parent'] == 0 || $value['id_parent_parent'] == 0) {
      $listCompanies[] = array("id"=>$value['id'],
      "name"=>$value['name'],
      "description"=>$value['description']);
    }
  }
  return $listCompanies;
}


// Carrega todos os prédios de uma resposta JSON para suas respectivas empresas
function setBuildingsFromCompanies(&$companies, $buildings_json)
{
  foreach ($companies as $key => &$company) {
    $buildings = array();
    foreach ($buildings_json as $key => $building_json) {
      if ($building_json['id_parent'] != $company['id']) {
        continue;
      }
      $buildings[] = array("id"=>$building_json['id'],
      "name"=>$building_json['name'],
      "description"=>$building_json['description']);
    }
    $company['buildings'] = $buildings;
  }
}


// Carrega todos os andares de uma resposta JSON para seus respectivos prédios
function setFloorsFromBuildings(&$companies, $floors_json)
{
  foreach ($companies as $key => &$company) {
    foreach ($company['buildings'] as $key => &$building) {
      $floors = array();
      foreach ($floors_json as $key => $floor_json) {
        if ($floor_json['id_parent'] != $building['id']) {
          continue;
        }
        $floors[] = array("id"=>$floor_json['id'],
        "name"=>$floor_json['name'],
        "description"=>$floor_json['description'],
        "number"=>1);
      }
      $building['floors'] = $floors;
    }
  }
}


// Carrega todas as salas de uma resposta JSON para seus respectivos andares
function setRoomsFromFloors(&$companies, $rooms_json)
{
  foreach ($companies as $key => &$company) {
    foreach ($company['buildings'] as $key => &$building) {
      foreach ($building['floors'] as $key => &$floor) {
        $rooms = array();
        foreach ($rooms_json as $key => $room_json) {
          if ($room_json['id_parent'] != $floor['id']) {
            continue;
          }
          $rooms[] = array("id"=>$room_json['id'],
          "name"=>$room_json['name'],
          "description"=>$room_json['description']);
        }
        $floor['rooms'] = $rooms;
      }
    }
  }
}


// Carrega todos os racks de uma resposta JSON para suas respectivas salas
function setRacksFromRooms(&$companies, $racks_json)
{
  foreach ($companies as $key => &$company) {
    foreach ($company['buildings'] as $key => &$building) {
      foreach ($building['floors'] as $key => &$floor) {
        foreach ($floor['rooms'] as $key => &$room) {
          $groups_racks = isset($room['group_racks']) ? $room['group_racks'] : array();
          $racks = isset($room['racks']) ? $room['racks'] : array();
          foreach ($racks_json as $key => $rack) {
            # Verifica se o rack pertence à sala
            if ($rack['id_parent'] == $room['id']) {
              $racks[] = array("id"=>$rack['id_rack'],
              "name"=>$rack['name_rack'],
              "description"=>$rack['description_rack'],
              "zone"=>$rack['zone'],
              "position_zone"=>$rack['position_zone'],
              "total_ru"=>$rack['total_ru'],
              "created_date"=>$rack['created_date'],
              "group_rack"=>array(),
              "potency_watts_limit"=>(($rack['potency_watts_limit'] == "0" || $rack['potency_watts_limit'] == null || $rack['potency_watts_limit'] == ".00") ? 0 : floatval($rack['potency_watts_limit'])),
              "electrical_sockets_limit"=>(($rack['electrical_sockets_limit'] == "0" || $rack['electrical_sockets_limit'] == null || $rack['electrical_sockets_limit'] == ".00") ? 0 : floatval($rack['electrical_sockets_limit'])));
              continue;
            }
            # Verifica se o rack está em um grupo de rack
            if ($rack['parent_type_name'] != "Rack Group") {
              continue;
            }
            # Busca mais informações sobre o grupo de rack e verifica se está na sala
            $group_rack = null;
            foreach ($groups_racks as $key => &$gr) {
              if ($gr['id'] == $rack['id_parent']) {
                $group_rack = $gr;
                break;
              }
            }
            if (is_null($group_rack)) {
              if ($rack['id_parent_parent'] != $room['id']) {
                continue;
              }
              $group_rack = array("id"=>$rack['id_parent'], "name"=>$rack['name_parent'],
              "description"=>$rack['description_parent']);
              $groups_racks[] = $group_rack;
            }
            # Adiciona o rack
            $racks[] = array("id"=>$rack['id_rack'],
            "name"=>$rack['name_rack'],
            "description"=>$rack['description_rack'],
            "zone"=>$rack['zone'],
            "position_zone"=>$rack['position_zone'],
            "total_ru"=>$rack['total_ru'],
            "created_date"=>$rack['created_date'],
            "group_rack"=>$group_rack,
            "potency_watts_limit"=>(($rack['potency_watts_limit'] == "0" || $rack['potency_watts_limit'] == null || $rack['potency_watts_limit'] == ".00") ? 0 : floatval($rack['potency_watts_limit'])),
            "electrical_sockets_limit"=>(($rack['eletrictal_sockets_limit'] == "0" || $rack['eletrictal_sockets_limit'] == null || $rack['eletrictal_sockets_limit'] == ".00") ? 0 : floatval($rack['eletrictal_sockets_limit'])));
          }
          $room['group_racks'] = $groups_racks;
          $room['racks'] = $racks;
        }
      }
    }
  }
}


// Carrega todos os equipamentos de uma resposta JSON para seus respectivos racks
function setEquipamentsFromRacks($con, &$companies, $equipaments_json)
{
  foreach ($companies as $key => &$company) {
    foreach ($company['buildings'] as $key => &$building) {
      foreach ($building['floors'] as $key => &$floor) {
        foreach ($floor['rooms'] as $key => &$room) {
          foreach ($room['racks'] as $key => &$rack) {
            $equipaments = isset($rack['equipaments']) ? $rack['equipaments'] : array();
            foreach ($equipaments_json as $key => $equipament_json) {
              if ($equipament_json['id_parent'] != $rack['id']) {
                continue;
              }
              $equipaments[] = $equipament_json;
            }
            $rack['equipaments'] = $equipaments;
          }
        }
      }
    }
  }
}


// Consulta e retorna todas as portas de um determinado equipamento
function getPorts($con, $equipament, $pathpanel=false)
{
  try {
    $ports = array();

    //if (!$pathpanel){

    $sql = "SELECT port.ID as id,
    port.Name as name,
    port.Description as description,
    port.Position as position,
    velocity.Name as velocity,
    service.Name as service,
    porttype.Name as type,
    id_slot_by_api = CASE slot.ID  WHEN $equipament THEN NULL ELSE slot.ID END,
    slot_name = CASE slot.ID  WHEN $equipament THEN NULL ELSE slot.Name END,
    slot_description = CASE slot.ID WHEN $equipament THEN NULL ELSE slot.Description END
    FROM SMEndpoint AS port
    LEFT JOIN SMParameterType AS velocity ON velocity.PTypeID = port.PortConfiguration
    LEFT JOIN AIMV_PortsWithDetail AS detail ON detail.ID = port.ID
    LEFT JOIN SMParameterType AS porttype ON porttype.PTypeID = detail.PortType
    LEFT JOIN SMService AS service ON service.ID = port.Service
    INNER JOIN SMObject AS slot ON slot.ID = port.ParentID
    WHERE slot.ParentID = $equipament OR port.ParentID = $equipament";

    $result = sqlsrv_query($con, $sql);

    while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {
      $port = $row;
      $port['status'] = getStatus($port['CableStatus'],
      $port['PatchStatus'],
      $port['PendingStatus'],
      $port['Reserved'],
      $port['Broken'],
      $port['Critical']);
      $ports[] = $port;
    }

    /*}else{

    $sql = "SELECT port.ID as id,
    port.Name as name,
    port.Description as description,
    port.Position as position,
    velocity.Name as velocity,
    service.Name as service,
    porttype.Name as type
    FROM SMEndpoint AS port
    LEFT JOIN SMParameterType AS velocity ON velocity.PTypeID = port.PortConfiguration
    LEFT JOIN AIMV_PortsWithDetail AS detail ON detail.ID = port.ID
    LEFT JOIN SMParameterType AS porttype ON porttype.PTypeID = detail.PortType
    LEFT JOIN SMService AS service ON service.ID = port.Service
    WHERE port.ParentID = ".$equipament;

    $result = sqlsrv_query($con, $sql);

    while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)){
    $port = $row;
    $port["status"] = null;
    $port["id_slot_by_api"] = null;
    $port["slot_name"] = null;
    $port["slot_description"] = null;
    $ports[] = $port;
  }

}*/

return $ports;

} catch (Exception $ex) {
  throw $ex;

} finally {
  if ($con) {
    sqlsrv_free_stmt($result);
  }
}

}


// Consulta e retorna as empresas cadastradas
function getListCompanies($con)
{
  try {
    $data = array();
    $sql = "SELECT city.ID as id,
    cityMore.Name as name,
    cityMore.Description as description,
    cityMore.ParentID as id_parent,
    cityMoreParent.ParentID as id_parent_parent
    FROM SMCity as city
    INNER JOIN SMObject AS cityMore ON cityMore.ID = city.ID
    INNER JOIN SMObject AS cityMoreParent ON cityMoreParent.ID = cityMore.ParentID";
    $result = sqlsrv_query($con, $sql);
    while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {
      $data[] = $row;
    }

    $sql2 = "SELECT ID as id,
    Name as name,
    Description as description,
    ParentID as id_parent
    FROM SMObject
    WHERE ParentID = 0";
    $result2 = sqlsrv_query($con, $sql2);
    while ($row = sqlsrv_fetch_array($result2, SQLSRV_FETCH_ASSOC)) {
      $data[] = $row;
    }

    return $data;

  } catch (Exception $ex) {
    throw $ex;

  } finally {
    if ($con) {
      sqlsrv_free_stmt($result);
      sqlsrv_free_stmt($result2);
    }

  }
}


// Consulta e retorna os prédios cadastrados
function getBuildings($con)
{
  try {
    $data = array();
    $sql = "SELECT building.ID as id,
    buildingMore.Name as name,
    buildingMore.Description as description,
    buildingMore.ParentID as id_parent
    FROM SMBuilding as building
    INNER JOIN SMObject AS buildingMore ON buildingMore.ID = building.ID";
    $result = sqlsrv_query($con, $sql);
    while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {
      $data[] = $row;
    }
    return $data;

  } catch (Exception $ex) {
    throw $ex;

  } finally {
    if ($con) {
      sqlsrv_free_stmt($result);
    }

  }
}


// Consulta e retorna todos os andares cadastrados
function getFloors($con)
{
  try {
    $data = array();
    $sql = "SELECT f.ID as id,
    floorMore.Name as name,
    floorMore.Description as description,
    floorMore.ParentID as id_parent
    FROM SMFloor as f
    INNER JOIN SMObject AS floorMore ON floorMore.ID = f.ID";
    $result = sqlsrv_query($con, $sql);
    while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {
      $data[] = $row;
    }
    return $data;

  } catch (Exception $ex) {
    throw $ex;

  } finally {
    if ($con) {
      sqlsrv_free_stmt($result);
    }

  }
}


// Consulta e retorna todas as salas cadastradas
function getRooms($con)
{
  try {
    $data = array();
    $sql = "SELECT room.ID as id,
    roomMore.Name as name,
    roomMore.Description as description,
    roomMore.ParentID as id_parent
    FROM SMRoom as room
    INNER JOIN SMObject AS roomMore ON roomMore.ID = room.ID";
    $result = sqlsrv_query($con, $sql);
    while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {
      $data[] = $row;
    }
    return $data;

  } catch (Exception $ex) {
    throw $ex;

  } finally {
    if ($con) {
      sqlsrv_free_stmt($result);
    }

  }
}


// Consulta e retorna todos os racks cadastrados
function getRacks($con, $capacity_fields)
{
  try {
    $data = array();
    $sql = "SELECT rack.ID as id_rack,
    rackMore.Name as name_rack,
    rackMore.Description as description_rack,
    rackMore.UCapacity as total_ru,
    rackMore.zone as zone,
    rackMore.Position as position_zone,
    rack.PowerLimit as PowerLimit,
    rack.WeightLimit as WeightLimit,
    rackMore.ParentID as id_parent,
    rackMore.ParentObjectTypeName as parent_type_name,
    objetoParent.Name as name_parent,
    objetoParent.Description as description_parent,
    objetoParent.ParentID as id_parent_parent,
    objeto.CreatedDate as created_date
    FROM SMRack as rack
    INNER JOIN AIMV_ContainersWithDetail as rackMore ON rackMore.ID = rack.ID
    INNER JOIN SMObject as objeto ON objeto.ID = rack.ID
    INNER JOIN SMObject as objetoParent ON objetoParent.ID = rackMore.ParentID";
    $result = sqlsrv_query($con, $sql);
    while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {
      $rack = $row;

      if ($capacity_fields == false) {
        $rack['potency_watts_limit'] = 0;
        $rack['electrical_sockets_limit'] = 0;
      } else {
        if ($capacity_fields['potency'] == false) {
          $rack['potency_watts_limit'] = 0;
        } else {
          $rack['potency_watts_limit'] = $rack[$capacity_fields['potency']['rack']];
        }

        if ($capacity_fields['electrical'] == false) {
          $rack['electrical_sockets_limit'] = 0;
        } else {
          $rack['electrical_sockets_limit'] = $rack[$capacity_fields['electrical']['rack']];
        }
      }

      unset($rack['PowerLimit']);
      unset($rack['WeightLimit']);

      $data[] = $rack;
    }

    return $data;

  } catch (Exception $ex) {
    throw $ex;

  } finally {
    if ($con) {
      sqlsrv_free_stmt($result);
    }

  }
}


// Consulta e retorna todos os equipamentos cadastrados
function getEquipaments($con, $capacity_fields)
{
  try {
    $data = array();

    $sql = "SELECT DISTINCT pathpanel.ID as id,
    pathpanel.Name as name,
    pathpanel.Description as description,
    pathpanel.Position as ru_top,
    pathpanel.UCapacity as ru_size,
    ((pathpanel.Position + pathpanel.UCapacity) -1) as ru_buttom,
    pathpanel.ResourceTypeName as resource_type_name,
    pathpanel.ParentID as id_parent,
    objeto.CreatedDate as created_date
    FROM AIMV_PatchPanelsWithDetail as pathpanel
    LEFT JOIN SMObject as objeto ON objeto.ID = pathpanel.ID";

    $result2 = sqlsrv_query($con, $sql);
    while ($row = sqlsrv_fetch_array($result2, SQLSRV_FETCH_ASSOC)) {
      $pathpanel = $row;
      $pathpanel['potency_watts_usage'] = 0;
      $pathpanel['electrical_sockets_usage'] = 0;
      $pathpanel['service'] = null;
      $pathpanel['ports'] = array();
      $data[] = $pathpanel;
    }


    $sql = "SELECT DISTINCT pathpanel.ID as id,
    pathpanel.Name as name,
    pathpanel.Description as description,
    pathpanel.Position as ru_top,
    pathpanel.UCapacity as ru_size,
    ((pathpanel.Position + pathpanel.UCapacity) -1) as ru_buttom,
    resource_type_name = CASE WHEN (pathpanel.ObjectTypeName = 'Blade Enclosure') THEN 'BladeEnclosure'
    ELSE 'PatchPanel'
    END,
    PowerUsage = CASE WHEN blade.PowerUsage IS NULL THEN 0 ELSE convert(float, blade.PowerUsage) END,
    WeightUsage = CASE WHEN blade.WeightUsage IS NULL THEN 0 ELSE convert(float, blade.WeightUsage) END,
    pathpanel.ParentID as id_parent,
    objeto.CreatedDate as created_date
    FROM AIMV_ClosuresWithDetail as pathpanel
    LEFT JOIN SMBladeEnclosure as blade ON blade.ID = pathpanel.ID
    LEFT JOIN SMObject as objeto ON objeto.ID = pathpanel.ID";

    $result3 = sqlsrv_query($con, $sql);
    while ($row = sqlsrv_fetch_array($result3, SQLSRV_FETCH_ASSOC)) {
      $pathpanel = $row;

      if ($capacity_fields == false) {
        $pathpanel['potency_watts_usage'] = 0;
        $pathpanel['electrical_sockets_usage'] = 0;
      } else {
        if ($capacity_fields['potency'] == false) {
          $pathpanel['potency_watts_usage'] = 0;
        } else {
          $pathpanel['potency_watts_usage'] = $pathpanel[$capacity_fields['potency']['equipament']];
        }
        if ($capacity_fields['electrical'] == false) {
          $pathpanel['electrical_sockets_usage'] = 0;
        } else {
          $pathpanel['electrical_sockets_usage'] = $pathpanel[$capacity_fields['electrical']['equipament']];
        }
      }
      unset($pathpanel['PowerUsage']);
      unset($pathpanel['WeightUsage']);
      $pathpanel['service'] = null;

      $pathpanel['ports'] = array();

      $data[] = $pathpanel;
    }


    $sql = "SELECT DISTINCT networkdevices.ID as id,
    networkdevices.Name as name,
    networkdevices.Description as description,
    networkdevices.Position as ru_top,
    networkdevices.UCapacity as ru_size,
    objeto.CreatedDate as created_date,
    ((networkdevices.Position + networkdevices.UCapacity) -1) as ru_buttom,
    REPLACE(networkdevices.ObjectTypeName, ' ', '') as resource_type_name,
    networkdevices.ParentID as id_parent,
    object.ParentID as id_parent_parent,
    PowerUsage = CASE WHEN (switch.PowerUsage IS NULL AND server.PowerUsage IS NULL) THEN 0
    WHEN switch.PowerUsage IS NULL THEN  convert(float,server.PowerUsage)
    ELSE  convert(float,switch.PowerUsage)
    END,
    WeightUsage = CASE WHEN (switch.WeightUsage IS NULL AND server.WeightUsage IS NULL) THEN 0
    WHEN switch.WeightUsage IS NULL THEN  convert(float, server.WeightUsage)
    ELSE  convert(float,switch.WeightUsage)
    END
    FROM AIMV_NetworkDevicesWithDetails as networkdevices
    LEFT JOIN SMSwitch as switch ON switch.ID = networkdevices.ID
    LEFT JOIN SMServer as server ON server.ID = networkdevices.ID
    LEFT JOIN SMObject as object ON object.ID = networkdevices.ParentID
    LEFT JOIN SMObject as objeto ON objeto.ID = networkdevices.ID";

    $result = sqlsrv_query($con, $sql);
    while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {
      $networkdevice = $row;

      $networkdevice['ports'] = array();

      $networkdevice['service'] = null;

      if ($networkdevice['resource_type_name'] == "BladeServer"){
        $networkdevice['potency_watts_usage'] = 0;
        $networkdevice['electrical_sockets_usage'] = 0;
        $networkdevice['ru_top'] = null;
        $networkdevice['ru_size'] = null;
        $networkdevice['ru_buttom'] = null;
        $networkdevice['id_equipament_parent'] = $networkdevice['id_parent'];
        $networkdevice['id_parent'] = $networkdevice['id_parent_parent'];
      }else{
        if ($capacity_fields == false) {
          $networkdevice['potency_watts_usage'] = 0;
          $networkdevice['electrical_sockets_usage'] = 0;
        } else {
          if ($capacity_fields['potency'] == false) {
            $networkdevice['potency_watts_usage'] = 0;
          } else {
            $networkdevice['potency_watts_usage'] = $networkdevice[$capacity_fields['potency']['equipament']];
          }
          if ($capacity_fields['electrical'] == false) {
            $networkdevice['electrical_sockets_usage'] = 0;
          } else {
            $networkdevice['electrical_sockets_usage'] = $networkdevice[$capacity_fields['electrical']['equipament']];
          }
        }
      }

      unset($networkdevice['PowerUsage']);
      unset($networkdevice['WeightUsage']);
      unset($networkdevice['id_parent_parent']);

      $data[] = $networkdevice;

    }

    // Pega todos os PatchCords
    $sql = "SELECT patchcord.ID as id,
    positionInRack.Name AS name,
    positionInRack.Description AS description,
    positionInRack.Position as ru_top,
    positionInRack.RackUnits as ru_size,
    ((positionInRack.Position + positionInRack.RackUnits) -1) as ru_buttom,
    positionInRack.ParentID as id_parent,
    objeto.CreatedDate as created_date
    FROM SMPatchCordOrganizer AS patchcord
    INNER JOIN SMV_ObjectUnderRack AS positionInRack ON positionInRack.ID = patchcord.ID
    LEFT JOIN SMObject as objeto ON objeto.ID = patchcord.ID";

    $result4 = sqlsrv_query($con, $sql);
    while ($row = sqlsrv_fetch_array($result4, SQLSRV_FETCH_ASSOC)) {
      $patchcord = $row;
      $patchcord['potency_watts_usage'] = 0;
      $patchcord['electrical_sockets_usage'] = 0;
      $patchcord['service'] = null;
      $patchcord['resource_type_name'] = "PatchCordOrganizer";
      $patchcord['ports'] = array();
      $data[] = $patchcord;
    }

    // Pega todos os ReservedRUs
    $sql = "SELECT reservedru.ID as id,
    positionInRack.Name AS name,
    positionInRack.Description AS description,
    positionInRack.Position as ru_top,
    positionInRack.RackUnits as ru_size,
    ((positionInRack.Position + positionInRack.RackUnits) -1) as ru_buttom,
    positionInRack.ParentID as id_parent,
    objeto.CreatedDate as created_date
    FROM SMReservedRU AS reservedru
    INNER JOIN SMV_ObjectUnderRack AS positionInRack ON positionInRack.ID = reservedru.ID
    LEFT JOIN SMObject as objeto ON objeto.ID = reservedru.ID";

    $result5 = sqlsrv_query($con, $sql);
    while ($row = sqlsrv_fetch_array($result5, SQLSRV_FETCH_ASSOC)) {
      $reservedRU = $row;
      $reservedRU['potency_watts_usage'] = 0;
      $reservedRU['electrical_sockets_usage'] = 0;
      $reservedRU['service'] = null;
      $reservedRU['resource_type_name'] = "ReservedRU";
      $reservedRU['ports'] = array();
      $data[] = $reservedRU;
    }

    return $data;

  } catch (Exception $ex) {
    throw $ex;
  } finally {
    if ($con) {
      sqlsrv_free_stmt($result);
      sqlsrv_free_stmt($result2);
      sqlsrv_free_stmt($result3);
      sqlsrv_free_stmt($result4);
      sqlsrv_free_stmt($result5);
    }
  }
}


function getPortsAll($con)
{
  try {

    //$ports = array();

    $sql = "SELECT port.ID as id,
    port.Name as name,
    port.Description as description,
    port.Position as position,
    port.CableStatus,
    port.PatchStatus,
    port.PendingStatus,
    port.Reserved,
    port.Broken,
    port.Critical,
    port.Path,
    velocity.Name as velocity,
    service.Name as service,
    porttype.Name as type,
    obj.ID as id_parent,
    obj.Name as name_parent,
    obj.Description as description_parent,
    LOWER(type1.Name) as system_type_object_parent,
    obj2.ID as id_parent_parent,
    LOWER(type.Name) as system_type_object_parent_parent,
    obj3.ID as id_parent_parent_parent,
    LOWER(type2.Name) as system_type_object_parent_parent_parent
    FROM SMEndpoint AS port
    LEFT JOIN SMParameterType AS velocity ON velocity.PTypeID = port.PortConfiguration
    LEFT JOIN AIMV_PortsWithDetail AS detail ON detail.ID = port.ID
    LEFT JOIN SMParameterType AS porttype ON porttype.PTypeID = detail.PortType
    LEFT JOIN SMService AS service ON service.ID = port.Service
    INNER JOIN SMObject AS obj ON obj.ID = port.ParentID
    INNER JOIN SMObject AS obj2 ON obj2.ID = obj.ParentID
    INNER JOIN SMObject AS obj3 ON obj3.ID = obj2.ParentID

    INNER JOIN SMObjectType as ot1 ON ot1.ID = obj.Type
    INNER JOIN SMSystemObjectType as type1 ON type1.ID = ot1.InheritID

    INNER JOIN SMObjectType as ot ON ot.ID = obj2.Type
    INNER JOIN SMSystemObjectType as type ON type.ID = ot.InheritID
    INNER JOIN SMObjectType as ot2 ON ot2.ID = obj3.Type
    INNER JOIN SMSystemObjectType as type2 ON type2.ID = ot2.InheritID";

    $result = sqlsrv_query($con, $sql);
    $data = array();

    while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {
      $port = $row;

      /*$port['status'] = getStatus($port['CableStatus'],
      $port['PatchStatus'],
      $port['PendingStatus'],
      $port['Reserved'],
      $port['Broken'],
      $port['Critical']);*/

      if ($port['system_type_object_parent_parent'] == "rack" || $port['system_type_object_parent_parent'] == "blade enclosure") {

        $port['status'] = getStatus($port['CableStatus'],
                                    $port['PatchStatus'],
                                    $port['PendingStatus'],
                                    $port['Reserved'],
                                    $port['Broken'],
                                    $port['Critical'],
                                    ($port['system_type_object_parent'] == "network equipment" ? "switch" : "patchpanel"));

        $flag = false;

        foreach ($data as $key => &$equipament) {
          if ($equipament['id'] == $port['id_parent']){
            $equipament['ports'][] = array("id"=>$port['id'],
            "name"=>$port['name'],
            "description"=>$port['description'],
            "position"=>$port['position'],
            "velocity"=>$port['velocity'],
            "service"=>$port['service'],
            "type_port"=>$port['type'],
            "id_parent"=>$port['id_parent'],
            "id_slot_by_api"=>null,
            "slot_name"=>null,
            "slot_description"=>null,
            "status"=>$port['status']);
            $flag = true;
            break;
          }
        }

        if (!$flag) {

          $ports = array();
          $ports[] = array("id"=>$port['id'],
          "name"=>$port['name'],
          "description"=>$port['description'],
          "position"=>$port['position'],
          "velocity"=>$port['velocity'],
          "service"=>$port['service'],
          "type_port"=>$port['type'],
          "id_parent"=>$port['id_parent'],
          "id_slot_by_api"=>null,
          "slot_name"=>null,
          "slot_description"=>null,
          "status"=>$port['status']);

          $data[] = array("id"=>$port['id_parent'], "ports"=>$ports);

        }

      } else if ($port['system_type_object_parent_parent_parent'] == "rack" || $port['system_type_object_parent_parent_parent'] == "blade enclosure" ){

        $port['status'] = getStatus($port['CableStatus'],
                                    $port['PatchStatus'],
                                    $port['PendingStatus'],
                                    $port['Reserved'],
                                    $port['Broken'],
                                    $port['Critical'],
                                    ($port['system_type_object_parent_parent'] == "network equipment" ? "switch" : "patchpanel"));

        $flag = false;

        foreach ($data as $key => &$equipament) {
          if ($equipament['id'] == $port['id_parent_parent']){
            $equipament['ports'][] = array("id"=>$port['id'],
            "name"=>$port['name'],
            "description"=>$port['description'],
            "position"=>$port['position'],
            "velocity"=>$port['velocity'],
            "service"=>$port['service'],
            "type_port"=>$port['type'],
            "status"=>$port['status'],
            "id_parent"=>$port['id_parent_parent'],
            "id_slot_by_api"=>$port['id_parent'],
            "slot_name"=>$port['name_parent'],
            "slot_description"=>$port['description_parent']);
            $flag = true;
            break;
          }
        }

        if (!$flag){

          $ports = array();
          $ports[] = array("id"=>$port['id'],
          "name"=>$port['name'],
          "description"=>$port['description'],
          "position"=>$port['position'],
          "velocity"=>$port['velocity'],
          "service"=>$port['service'],
          "type_port"=>$port['type'],
          "status"=>$port['status'],
          "id_parent"=>$port['id_parent_parent'],
          "id_slot_by_api"=>$port['id_parent'],
          "slot_name"=>$port['name_parent'],
          "slot_description"=>$port['description_parent']);

          $data[] = array("id"=>$port['id_parent_parent'], "ports"=>$ports);

        }

      }

    }

    return $data;

  } catch (Exception $ex) {
    throw $ex;
  } finally {
    if ($con) {
      sqlsrv_free_stmt($result);
    }
  }
}




try {

  $ip_database = $_GET['ip-database'];
  $user_database = $_GET['user-database'];
  $password_database = $_GET['password-database'];

  $con = sqlsrv_connect($ip_database, array("Database" => NAME_DATABASE, "Uid" => $user_database, "PWD" => $password_database));

  $capacity_fields = getColumnsCapacity($con);

  $companies_json = getListCompanies($con);
  $buildings_json = getBuildings($con);
  $floors_json = getFloors($con);
  $rooms_json = getRooms($con);
  $racks_json = getRacks($con, $capacity_fields);
  $equipaments_json = getEquipaments($con, $capacity_fields);
  $ports_all = getPortsAll($con);

  $companies = getCompanies($companies_json);
  setBuildingsFromCompanies($companies, $buildings_json);
  setFloorsFromBuildings($companies, $floors_json);
  setRoomsFromFloors($companies, $rooms_json);
  setRacksFromRooms($companies, $racks_json);
  setEquipamentsFromRacks($con, $companies, $equipaments_json);

  echo json_encode(array("companies"=>$companies, "ports"=>$ports_all));

} catch (Exception $ex) {
  $data = array("status"=>"failed");
  echo json_encode($data);

} finally {
  if ($con) {
    sqlsrv_close($con);
  }

}

?>
