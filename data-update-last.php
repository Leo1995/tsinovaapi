<?php

header("Content-type: application/json");

include "config.php";
include "util.php";

// Método que verifica se um determinado componente é um rack
function isRack($con, $id)
{
    try {
        $sql = "SELECT LOWER(sobt.Name) as system_type_object
                FROM SMObject as obj
                INNER JOIN SMObjectType as obt ON obt.ID = obj.Type
                INNER JOIN SMSystemObjectType as sobt ON sobt.ID = obt.InheritID
                WHERE obj.ID = $id";
        $result = sqlsrv_query($con, $sql);
        while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {
            return ($row['system_type_object'] == "rack");
        }
        return false;
    } catch (Exception $ex) {
        throw $ex;
    } finally {
        if ($con) {
            sqlsrv_free_stmt($result);
        }
    }
}


// Método que retorna as informações de um rack através de seu ID
function getRackByID($con, $id, $capacity_fields)
{
    try {
        $data = null;
        $sql = "SELECT rack.ID as id_rack,
                       rackMore.Name as name_rack,
                       rackMore.Description as description_rack,
                       rackMore.UCapacity as total_ru,
                       rackMore.zone as zone,
                       rackMore.Position as position_zone,
                       rack.PowerLimit as PowerLimit,
                       rack.WeightLimit as WeightLimit,
                       rackMore.ParentID as id_parent,
                       rackMore.ParentObjectTypeName as parent_type_name,
                       objetoParent.Name as name_parent,
                       objetoParent.Description as description_parent,
                       objetoParent.ParentID as id_parent_parent,
                       objeto.CreatedDate as created_date
                  FROM SMRack as rack
                  INNER JOIN AIMV_ContainersWithDetail as rackMore ON rackMore.ID = rack.ID
                  INNER JOIN SMObject as objeto ON objeto.ID = rack.ID
                  INNER JOIN SMObject as objetoParent ON objetoParent.ID = rackMore.ParentID
                  WHERE rack.ID = $id";
        $result = sqlsrv_query($con, $sql);
        while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {
            $data = $row;
            if ($capacity_fields == false) {
                $data['potency_watts_limit'] = 0;
                $data['electrical_sockets_limit'] = 0;
            } else {
                if ($capacity_fields['potency'] == false) {
                    $data['potency_watts_limit'] = 0;
                } else {
                    $data['potency_watts_limit'] = $data[$capacity_fields['potency']['rack']];
                }

                if ($capacity_fields['electrical'] == false) {
                    $data['electrical_sockets_limit'] = 0;
                } else {
                    $data['electrical_sockets_limit'] = $data[$capacity_fields['electrical']['rack']];
                }
            }
            unset($data['PowerLimit']);
            unset($data['WeightLimit']);
        }

        return $data;
    } catch (Exception $ex) {
        throw $ex;
    } finally {
        if ($con) {
            sqlsrv_free_stmt($result);
        }
    }
}


// Método que retorna as informações de uma determinada porta através de seu ID
function getPort($con, $id)
{
    try {
        $port = null;

        $sql = "SELECT port.ID as id,
                      port.Name as name,
                      port.Description as description,
                      port.Position as position,
                      port.CableStatus,
                      port.PatchStatus,
                      port.PendingStatus,
                      port.Reserved,
                      port.Broken,
                      port.Critical,
                      velocity.Name as velocity,
                      service.Name as service,
                      porttype.Name as type,
                      obj.ID as id_parent,
                      obj.Name as name_parent,
                      obj.Description as description_parent,
                      LOWER(type1.Name) as system_type_object_parent,
                      obj2.ID as id_parent_parent,
                      LOWER(type.Name) as system_type_object_parent_parent,
                      obj3.ID as id_parent_parent_parent,
                      LOWER(type2.Name) as system_type_object_parent_parent_parent
                  FROM SMEndpoint AS port
                  LEFT JOIN SMParameterType AS velocity ON velocity.PTypeID = port.PortConfiguration
                  LEFT JOIN AIMV_PortsWithDetail AS detail ON detail.ID = port.ID
                  LEFT JOIN SMParameterType AS porttype ON porttype.PTypeID = detail.PortType
                  LEFT JOIN SMService AS service ON service.ID = port.Service
                  INNER JOIN SMObject AS obj ON obj.ID = port.ParentID
                  INNER JOIN SMObject AS obj2 ON obj2.ID = obj.ParentID
                  INNER JOIN SMObject AS obj3 ON obj3.ID = obj2.ParentID

                  INNER JOIN SMObjectType as ot1 ON ot1.ID = obj.Type
                  INNER JOIN SMSystemObjectType as type1 ON type1.ID = ot1.InheritID

                  INNER JOIN SMObjectType as ot ON ot.ID = obj2.Type
                  INNER JOIN SMSystemObjectType as type ON type.ID = ot.InheritID
                  INNER JOIN SMObjectType as ot2 ON ot2.ID = obj3.Type
                  INNER JOIN SMSystemObjectType as type2 ON type2.ID = ot2.InheritID
                  WHERE port.ID = $id";

        $result = sqlsrv_query($con, $sql);

        while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {
            $port = $row;

            if ($port['system_type_object_parent_parent'] == "rack" || $port['system_type_object_parent_parent'] == "blade enclosure") {

              $port['status'] = getStatus($port['CableStatus'],
                                          $port['PatchStatus'],
                                          $port['PendingStatus'],
                                          $port['Reserved'],
                                          $port['Broken'],
                                          $port['Critical'],
                                          ($port['system_type_object_parent'] == "network equipment" ? "switch" : "patchpanel"));

            } else if ($port['system_type_object_parent_parent_parent'] == "rack" || $port['system_type_object_parent_parent_parent'] == "blade enclosure" ){

              $port['status'] = getStatus($port['CableStatus'],
                                          $port['PatchStatus'],
                                          $port['PendingStatus'],
                                          $port['Reserved'],
                                          $port['Broken'],
                                          $port['Critical'],
                                          ($port['system_type_object_parent_parent'] == "network equipment" ? "switch" : "patchpanel"));

            }

        }

        return $port;
    } catch (Exception $ex) {
        throw $ex;
    } finally {
        if ($con) {
            sqlsrv_free_stmt($result);
        }
    }
}


// Método que retorna todas as portas criadas após uma determinada data
function getPortsCreated($con, $date_last)
{
    try {

        $ports = array();

        $sql = "SELECT port.ID as id,
                       port.Name as name,
                       port.Description as description,
                       port.Position as position,
                       port.CableStatus,
                       port.PatchStatus,
                       port.PendingStatus,
                       port.Reserved,
                       port.Broken,
                       port.Critical,
                       port.Path,
                       velocity.Name as velocity,
                       service.Name as service,
                       porttype.Name as type,
                       CONVERT(VARCHAR, port.CreatedDate, 120) as date_log,
                       obj.ID as id_parent,
                       obj.Name as name_parent,
                       obj.Description as description_parent,
                       LOWER(type1.Name) as system_type_object_parent,
                       obj2.ID as id_parent_parent,
                       LOWER(type.Name) as system_type_object_parent_parent,
                       obj3.ID as id_parent_parent_parent,
                       LOWER(type2.Name) as system_type_object_parent_parent_parent
                FROM SMEndpoint AS port
                LEFT JOIN SMParameterType AS velocity ON velocity.PTypeID = port.PortConfiguration
                LEFT JOIN AIMV_PortsWithDetail AS detail ON detail.ID = port.ID
                LEFT JOIN SMParameterType AS porttype ON porttype.PTypeID = detail.PortType
                LEFT JOIN SMService AS service ON service.ID = port.Service
                INNER JOIN SMObject AS obj ON obj.ID = port.ParentID
                INNER JOIN SMObject AS obj2 ON obj2.ID = obj.ParentID
                INNER JOIN SMObject AS obj3 ON obj3.ID = obj2.ParentID

                INNER JOIN SMObjectType as ot1 ON ot1.ID = obj.Type
                INNER JOIN SMSystemObjectType as type1 ON type1.ID = ot1.InheritID

                INNER JOIN SMObjectType as ot ON ot.ID = obj2.Type
                INNER JOIN SMSystemObjectType as type ON type.ID = ot.InheritID
                INNER JOIN SMObjectType as ot2 ON ot2.ID = obj3.Type
                INNER JOIN SMSystemObjectType as type2 ON type2.ID = ot2.InheritID

                WHERE port.CreatedDate > '$date_last'";

        $result = sqlsrv_query($con, $sql);

        while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {
            $port = $row;

            if ($port['system_type_object_parent_parent'] == "rack" || $port['system_type_object_parent_parent'] == "blade enclosure") {

                $port['status'] = getStatus($port['CableStatus'],
                                            $port['PatchStatus'],
                                            $port['PendingStatus'],
                                            $port['Reserved'],
                                            $port['Broken'],
                                            $port['Critical'],
                                            ($port['system_type_object_parent'] == "network equipment" ? "switch" : "patchpanel"));

                $ports[] = array("id"=>$port['id'],
                                   "name"=>$port['name'],
                                   "description"=>$port['description'],
                                   "position"=>$port['position'],
                                   "velocity"=>$port['velocity'],
                                   "service"=>$port['service'],
                                   "type_port"=>$port['type'],
                                   "id_parent"=>$port['id_parent'],
                                   "id_slot_by_api"=>null,
                                   "slot_name"=>null,
                                   "slot_description"=>null,
                                   "status"=>$port['status'],
                                   "type"=>"port",
                                   "action"=>2,
                                   "date_log"=>$port['date_log']);

            } else if ($port['system_type_object_parent_parent_parent'] == "rack" || $port['system_type_object_parent_parent_parent'] == "blade enclosure" ){

                $port['status'] = getStatus($port['CableStatus'],
                                            $port['PatchStatus'],
                                            $port['PendingStatus'],
                                            $port['Reserved'],
                                            $port['Broken'],
                                            $port['Critical'],
                                            ($port['system_type_object_parent_parent'] == "network equipment" ? "switch" : "patchpanel"));

                $ports[] = array("id"=>$port['id'],
                                   "name"=>$port['name'],
                                   "description"=>$port['description'],
                                   "position"=>$port['position'],
                                   "velocity"=>$port['velocity'],
                                   "service"=>$port['service'],
                                   "type_port"=>$port['type'],
                                   "status"=>$port['status'],
                                   "id_parent"=>$port['id_parent_parent'],
                                   "id_slot_by_api"=>$port['id_parent'],
                                   "slot_name"=>$port['name_parent'],
                                   "slot_description"=>$port['description_parent'],
                                   "type"=>"port",
                                   "action"=>2,
                                   "date_log"=>$port['date_log']);

            }

        }

        return $ports;
    } catch (Exception $ex) {
        throw $ex;
    } finally {
        if ($con) {
            sqlsrv_free_stmt($result);
        }
    }
}


// Método que retorna as informações de um equipamento através de seu ID
function getEquipament($con, $id, $type, $capacity_fields)
{
    try {

        if ($type == "patch cord organizer"){
          return getPatchCordOrganizerByID($con, $id);
        }

        if ($type == "reserved ru"){
          return getReservedRUByID($con, $id);
        }

        $equipament = getPathPanelByID($con, $id, $capacity_fields);
        if ($equipament != null) {
            return $equipament;
        }

        $equipament = getNetworkDeviceByID($con, $id, $capacity_fields);

        return $equipament;

    } catch (Exception $ex) {
        throw $ex;
    }
}


// Método que retorna as informações de um PatchCordOrganizer através de seu ID
function getPatchCordOrganizerByID($con, $id){
    try {

        $sql = "SELECT patchcord.ID as id,
                       positionInRack.Name AS name,
                       positionInRack.Description AS description,
                       positionInRack.Position as ru_top,
                       positionInRack.RackUnits as ru_size,
                       ((positionInRack.Position + positionInRack.RackUnits) -1) as ru_buttom,
                       positionInRack.ParentID as id_parent
                FROM SMPatchCordOrganizer AS patchcord
                INNER JOIN SMV_ObjectUnderRack AS positionInRack ON positionInRack.ID = patchcord.ID
                WHERE patchcord.ID = $id";

        $result = sqlsrv_query($con, $sql);
        while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {
            $patchcord = $row;
            $patchcord['potency_watts_usage'] = 0;
            $patchcord['electrical_sockets_usage'] = 0;
            $patchcord['service'] = null;
            $patchcord['resource_type_name'] = "PatchCordOrganizer";
            return $patchcord;
        }

    } catch (Exception $ex) {
        throw $ex;
    } finally {
        if ($con) {
            sqlsrv_free_stmt($result);
        }
    }

}

// Método que retorna as informações de um ReservedRU através de seu ID
function getReservedRUByID($con, $id){
    try {

        $sql = "SELECT reservedru.ID as id,
                       positionInRack.Name AS name,
                       positionInRack.Description AS description,
                       positionInRack.Position as ru_top,
                       positionInRack.RackUnits as ru_size,
                       ((positionInRack.Position + positionInRack.RackUnits) -1) as ru_buttom,
                       positionInRack.ParentID as id_parent
                FROM SMReservedRU AS reservedru
                INNER JOIN SMV_ObjectUnderRack AS positionInRack ON positionInRack.ID = reservedru.ID
                WHERE reservedru.ID = $id";

        $result = sqlsrv_query($con, $sql);
        while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {
            $reservedRU = $row;
            $reservedRU['potency_watts_usage'] = 0;
            $reservedRU['electrical_sockets_usage'] = 0;
            $reservedRU['service'] = null;
            $reservedRU['resource_type_name'] = "ReservedRU";
            return $reservedRU;
        }

    } catch (Exception $ex) {
        throw $ex;
    } finally {
        if ($con) {
            sqlsrv_free_stmt($result);
        }
    }

}


// Método que retorna as informações de um PatchPanel através de seu ID
function getPathPanelByID($con, $id, $capacity_fields)
{
    try {
        $sql = "SELECT DISTINCT pathpanel.ID as id,
                       pathpanel.Name as name,
                       pathpanel.Description as description,
                       pathpanel.Position as ru_top,
                       pathpanel.UCapacity as ru_size,
                       ((pathpanel.Position + pathpanel.UCapacity) -1) as ru_buttom,
                       pathpanel.ResourceTypeName as resource_type_name,
                       pathpanel.ParentID as id_parent
                FROM AIMV_PatchPanelsWithDetail as pathpanel
                WHERE pathpanel.ID = $id";
        $result = sqlsrv_query($con, $sql);
        while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {
            $pathpanel = $row;
            $pathpanel['potency_watts_usage'] = 0;
            $pathpanel['electrical_sockets_usage'] = 0;
            $pathpanel['service'] = null;
            return $pathpanel;
        }

        $sql = "SELECT DISTINCT pathpanel.ID as id,
                       pathpanel.Name as name,
                       pathpanel.Description as description,
                       pathpanel.Position as ru_top,
                       pathpanel.UCapacity as ru_size,
                       ((pathpanel.Position + pathpanel.UCapacity) -1) as ru_buttom,
                       resource_type_name = CASE WHEN (pathpanel.ObjectTypeName = 'Blade Enclosure') THEN 'BladeEnclosure'
                                                  ELSE 'PatchPanel'
                                             END,
                       PowerUsage = CASE WHEN blade.PowerUsage IS NULL THEN 0 ELSE convert(float, blade.PowerUsage) END,
                       WeightUsage = CASE WHEN blade.WeightUsage IS NULL THEN 0 ELSE convert(float, blade.WeightUsage) END,
                       pathpanel.ParentID as id_parent
                FROM AIMV_ClosuresWithDetail as pathpanel
                LEFT JOIN SMBladeEnclosure as blade ON blade.ID = pathpanel.ID
                WHERE pathpanel.ID = $id";

        $result2 = sqlsrv_query($con, $sql);
        while ($row = sqlsrv_fetch_array($result2, SQLSRV_FETCH_ASSOC)) {
            $pathpanel = $row;
            $pathpanel['service'] = null;
            if ($capacity_fields == false) {
                $pathpanel['potency_watts_usage'] = 0;
                $pathpanel['electrical_sockets_usage'] = 0;
            } else {
                if ($capacity_fields['potency'] == false) {
                    $pathpanel['potency_watts_usage'] = 0;
                } else {
                    $pathpanel['potency_watts_usage'] = $pathpanel[$capacity_fields['potency']['equipament']];
                }
                if ($capacity_fields['electrical'] == false) {
                    $pathpanel['electrical_sockets_usage'] = 0;
                } else {
                    $pathpanel['electrical_sockets_usage'] = $pathpanel[$capacity_fields['electrical']['equipament']];
                }
            }
            unset($pathpanel['PowerUsage']);
            unset($pathpanel['WeightUsage']);
            return $pathpanel;
        }

    } catch (Exception $ex) {
        throw $ex;
    } finally {
        if ($con) {
            sqlsrv_free_stmt($result);
            if (isset($result2)) {
                sqlsrv_free_stmt($result2);
            }
        }
    }
}


// Método que retorna as informações de um equipamento de rede através de seu ID
function getNetworkDeviceByID($con, $id, $capacity_fields)
{
    try {
        $data = null;
        $sql = "SELECT DISTINCT networkdevices.ID as id,
                       networkdevices.Name as name,
                       networkdevices.Description as description,
                       networkdevices.Position as ru_top,
                       networkdevices.UCapacity as ru_size,
                       ((networkdevices.Position + networkdevices.UCapacity) -1) as ru_buttom,
                       REPLACE(networkdevices.ObjectTypeName, ' ', '') as resource_type_name,
                       networkdevices.ParentID as id_parent,
                       service.Name as service,
                       PowerUsage = CASE WHEN (switch.PowerUsage IS NULL AND server.PowerUsage IS NULL) THEN 0
                                         WHEN switch.PowerUsage IS NULL THEN  convert(float,server.PowerUsage)
                                         ELSE  convert(float,switch.PowerUsage)
                                    END,
                       WeightUsage = CASE WHEN (switch.WeightUsage IS NULL AND server.WeightUsage IS NULL) THEN 0
                                          WHEN switch.WeightUsage IS NULL THEN  convert(float, server.WeightUsage)
                                          ELSE  convert(float,switch.WeightUsage)
                                     END
                 FROM AIMV_NetworkDevicesWithDetails as networkdevices
                 LEFT JOIN SMSwitch as switch ON switch.ID = networkdevices.ID
                 LEFT JOIN SMServer as server ON server.ID = networkdevices.ID
                 LEFT JOIN SMService AS service ON service.ID = switch.Service
                 WHERE networkdevices.ID = $id";
        $result = sqlsrv_query($con, $sql);
        while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {
            $data = $row;
            if ($capacity_fields == false) {
                $data['potency_watts_usage'] = 0;
                $data['electrical_sockets_usage'] = 0;
            } else {
                if ($capacity_fields['potency'] == false) {
                    $data['potency_watts_usage'] = 0;
                } else {
                    $data['potency_watts_usage'] = $data[$capacity_fields['potency']['equipament']];
                }

                if ($capacity_fields['electrical'] == false) {
                    $data['electrical_sockets_usage'] = 0;
                } else {
                    $data['electrical_sockets_usage'] = $data[$capacity_fields['electrical']['equipament']];
                }
            }
            unset($data['PowerUsage']);
            unset($data['WeightUsage']);
        }

        return $data;
    } catch (Exception $ex) {
        throw $ex;
    } finally {
        if ($con) {
            sqlsrv_free_stmt($result);
        }
    }
}


// Método que retorna o serviço de um slot (cartão)
function getServiceCardByID($con, $id)
{
    try {
        $sql = "SELECT service.Name as service
                FROM SMCard as card
                LEFT JOIN SMService AS service ON service.ID = card.Service
                WHERE card.ID = $id";
        $result = sqlsrv_query($con, $sql);
        while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {
            return $row['service'];
        }
        return null;
    } catch (Exception $ex) {
        throw $ex;
    } finally {
        if ($con) {
            sqlsrv_free_stmt($result);
        }
    }
}


// Método que retorna todos os equipamentos modificados após uma daterminada data
function getEquipaments($con, $date_last, $capacity_fields)
{
    try {

        $data = array();

        // Pega todos os PatchPanels
        $sql = "SELECT DISTINCT pathpanel.ID as id,
                       pathpanel.Name as name,
                       pathpanel.Description as description,
                       pathpanel.Position as ru_top,
                       pathpanel.UCapacity as ru_size,
                       ((pathpanel.Position + pathpanel.UCapacity) -1) as ru_buttom,
                       pathpanel.ResourceTypeName as resource_type_name,
                       pathpanel.ParentID as id_parent,
                       CONVERT(VARCHAR, object.ModifiedDate, 120) as date_log
                FROM AIMV_PatchPanelsWithDetail as pathpanel
                INNER JOIN SMObject as object ON object.ID = pathpanel.ID
                WHERE object.ModifiedDate > '$date_last' AND object.ModifiedDate != object.CreatedDate";
        $result = sqlsrv_query($con, $sql);
        while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {
            $pathpanel = $row;
            $pathpanel['potency_watts_usage'] = 0;
            $pathpanel['electrical_sockets_usage'] = 0;
            $pathpanel['service'] = null;
            $pathpanel['action'] = 3;
            $pathpanel['type'] = "device";
            $data[] = $pathpanel;
        }

        // Pega todos os equipamentos de rede
        $sql = "SELECT DISTINCT networkdevices.ID as id,
                       networkdevices.Name as name,
                       networkdevices.Description as description,
                       networkdevices.Position as ru_top,
                       networkdevices.UCapacity as ru_size,
                       ((networkdevices.Position + networkdevices.UCapacity) -1) as ru_buttom,
                       REPLACE(networkdevices.ObjectTypeName, ' ', '') as resource_type_name,
                       networkdevices.ParentID as id_parent,
                       CONVERT(VARCHAR, object.ModifiedDate, 120) as date_log,
                       service.Name as service,
                       PowerUsage = CASE WHEN (switch.PowerUsage IS NULL AND server.PowerUsage IS NULL) THEN 0
                                                  WHEN switch.PowerUsage IS NULL THEN  convert(float,server.PowerUsage)
                                                  ELSE  convert(float,switch.PowerUsage)
                                             END,
                       WeightUsage = CASE WHEN (switch.WeightUsage IS NULL AND server.WeightUsage IS NULL) THEN 0
                                                       WHEN switch.WeightUsage IS NULL THEN  convert(float, server.WeightUsage)
                                                       ELSE  convert(float,switch.WeightUsage)
                                                  END
                 FROM AIMV_NetworkDevicesWithDetails as networkdevices
                 LEFT JOIN SMSwitch as switch ON switch.ID = networkdevices.ID
                 LEFT JOIN SMServer as server ON server.ID = networkdevices.ID
                 LEFT JOIN SMService AS service ON service.ID = switch.Service
                 INNER JOIN SMObject as object ON object.ID = networkdevices.ID
                 WHERE object.ModifiedDate > '$date_last' AND object.ModifiedDate != object.CreatedDate AND networkdevices.ObjectTypeName != 'Blade Server'";
        $result2 = sqlsrv_query($con, $sql);
        while ($row = sqlsrv_fetch_array($result2, SQLSRV_FETCH_ASSOC)) {
            $networkdevice = $row;
            $networkdevice['action'] = 3;
            $networkdevice['type'] = "device";

            if ($capacity_fields == false) {
                $networkdevice['potency_watts_usage'] = 0;
                $networkdevice['electrical_sockets_usage'] = 0;
            } else {
                if ($capacity_fields['potency'] == false) {
                    $networkdevice['potency_watts_usage'] = 0;
                } else {
                    $networkdevice['potency_watts_usage'] = $networkdevice[$capacity_fields['potency']['equipament']];
                }
                if ($capacity_fields['electrical'] == false) {
                    $networkdevice['electrical_sockets_usage'] = 0;
                } else {
                    $networkdevice['electrical_sockets_usage'] = $networkdevice[$capacity_fields['electrical']['equipament']];
                }
            }

            unset($networkdevice['PowerUsage']);
            unset($networkdevice['WeightUsage']);

            $data[] = $networkdevice;
        }


        // Pega todos os Closures (Os demais PatchPanels)
        $sql = "SELECT DISTINCT pathpanel.ID as id,
                       pathpanel.Name as name,
                       pathpanel.Description as description,
                       pathpanel.Position as ru_top,
                       pathpanel.UCapacity as ru_size,
                       ((pathpanel.Position + pathpanel.UCapacity) -1) as ru_buttom,
                       pathpanel.ParentID as id_parent,
                       resource_type_name = CASE WHEN (pathpanel.ObjectTypeName = 'Blade Enclosure') THEN 'BladeEnclosure'
                                                  ELSE 'PatchPanel'
                                             END,
                       PowerUsage = CASE WHEN blade.PowerUsage IS NULL THEN 0 ELSE convert(float, blade.PowerUsage) END,
                       WeightUsage = CASE WHEN blade.WeightUsage IS NULL THEN 0 ELSE convert(float, blade.WeightUsage) END,
                       CONVERT(VARCHAR, object.ModifiedDate, 120) as date_log
                 FROM AIMV_ClosuresWithDetail as pathpanel
                 INNER JOIN SMObject as object ON object.ID = pathpanel.ID
                 LEFT JOIN SMBladeEnclosure as blade ON blade.ID = pathpanel.ID
                 WHERE object.ModifiedDate > '$date_last' AND object.ModifiedDate != object.CreatedDate";
        $result3 = sqlsrv_query($con, $sql);
        while ($row = sqlsrv_fetch_array($result3, SQLSRV_FETCH_ASSOC)) {
            $pathpanel = $row;

            if ($capacity_fields == false) {
                $pathpanel['potency_watts_usage'] = 0;
                $pathpanel['electrical_sockets_usage'] = 0;
            } else {
                if ($capacity_fields['potency'] == false) {
                    $pathpanel['potency_watts_usage'] = 0;
                } else {
                    $pathpanel['potency_watts_usage'] = $pathpanel[$capacity_fields['potency']['equipament']];
                }
                if ($capacity_fields['electrical'] == false) {
                    $pathpanel['electrical_sockets_usage'] = 0;
                } else {
                    $pathpanel['electrical_sockets_usage'] = $pathpanel[$capacity_fields['electrical']['equipament']];
                }
            }
            unset($pathpanel['PowerUsage']);
            unset($pathpanel['WeightUsage']);

            $pathpanel['service'] = null;
            $pathpanel['action'] = 3;
            $pathpanel['type'] = "device";
            $data[] = $pathpanel;
        }

        // Pega todos os PatchCords
        $sql = "SELECT patchcord.ID as id,
                       positionInRack.Name AS name,
                       positionInRack.Description AS description,
                       positionInRack.Position as ru_top,
                       positionInRack.RackUnits as ru_size,
                       ((positionInRack.Position + positionInRack.RackUnits) -1) as ru_buttom,
                       positionInRack.ParentID as id_parent,
                       CONVERT(VARCHAR, positionInRack.ModifiedDate, 120) as date_log
                FROM SMPatchCordOrganizer AS patchcord
                INNER JOIN SMV_ObjectUnderRack AS positionInRack ON positionInRack.ID = patchcord.ID
                WHERE positionInRack.ModifiedDate > '$date_last' AND positionInRack.ModifiedDate != positionInRack.CreatedDate";
        $result4 = sqlsrv_query($con, $sql);
        while ($row = sqlsrv_fetch_array($result4, SQLSRV_FETCH_ASSOC)) {
            $patchcord = $row;
            $patchcord['potency_watts_usage'] = 0;
            $patchcord['electrical_sockets_usage'] = 0;
            $patchcord['service'] = null;
            $patchcord['resource_type_name'] = "PatchCordOrganizer";
            $patchcord['action'] = 3;
            $patchcord['type'] = "device";
            $data[] = $patchcord;
        }

        // Pega todos os ReservedRUs
        $sql = "SELECT reservedru.ID as id,
                       positionInRack.Name AS name,
                       positionInRack.Description AS description,
                       positionInRack.Position as ru_top,
                       positionInRack.RackUnits as ru_size,
                       ((positionInRack.Position + positionInRack.RackUnits) -1) as ru_buttom,
                       positionInRack.ParentID as id_parent,
                       CONVERT(VARCHAR, positionInRack.ModifiedDate, 120) as date_log
                FROM SMReservedRU AS reservedru
                INNER JOIN SMV_ObjectUnderRack AS positionInRack ON positionInRack.ID = reservedru.ID
                WHERE positionInRack.ModifiedDate > '$date_last' AND positionInRack.ModifiedDate != positionInRack.CreatedDate";
        $result5 = sqlsrv_query($con, $sql);
        while ($row = sqlsrv_fetch_array($result5, SQLSRV_FETCH_ASSOC)) {
            $reservedRU = $row;
            $reservedRU['potency_watts_usage'] = 0;
            $reservedRU['electrical_sockets_usage'] = 0;
            $reservedRU['service'] = null;
            $reservedRU['resource_type_name'] = "ReservedRU";
            $reservedRU['action'] = 3;
            $reservedRU['type'] = "device";
            $data[] = $reservedRU;
        }

        return $data;

    } catch (Exception $ex) {
        throw $ex;
    } finally {
        if ($con) {
            sqlsrv_free_stmt($result);
            sqlsrv_free_stmt($result2);
            sqlsrv_free_stmt($result3);
            sqlsrv_free_stmt($result4);
            sqlsrv_free_stmt($result5);
        }
    }
}


// Método que organiza a lista em ordem de date_log crescente
function order($array)
{
    $length = count($array);
    if ($length <= 1) {
        return $array;
    } else {
        $pivot = $array[0];
        $left = $right = array();
        for ($i = 1; $i < count($array); $i++) {
            if (new DateTime($array[$i]['date_log']) < new DateTime($pivot['date_log'])) {
                $left[] = $array[$i];
            } else {
                $right[] = $array[$i];
            }
        }
        return array_merge(order($left), array($pivot), order($right));
    }
}


try {

    $date_last = $_GET['date-last'];
    $ip_database = $_GET['ip-database'];
    $user_database = $_GET['user-database'];
    $password_database = $_GET['password-database'];

    $con = sqlsrv_connect($ip_database, array("Database" => NAME_DATABASE, "Uid" => $user_database, "PWD" => $password_database));
    $data_api = array();
    $capacity_fields = getColumnsCapacity($con);

    $sql = "SELECT logobject.ID as id_log,
                   logobject.ObjectId as id_object,
                   detail.ParentID as id_parent,
                   logobject.ObjectName  as name_object,
                   detail.description as description_object,
                   CONVERT(VARCHAR, logobject.ActionTime, 120) as date_log,
                   logobject.IdPath as path_object,
                   logobject.ActionType as type_action,
                   obt.Name as type_object,
                   obt.ID as id_type_object,
                   LOWER(sobt.Name) as system_type_object,
                   sobt.ID as id_system_type_object,
                   LOWER(sobt.PropertyTable) as table_system
            FROM SMUserActionLog as logobject
            INNER JOIN SMObjectType as obt ON obt.ID = logobject.ObjectType
            INNER JOIN SMSystemObjectType as sobt ON sobt.ID = obt.InheritID
            LEFT JOIN SMObject as detail ON detail.ID = logobject.ObjectId
            WHERE logobject.ActionTime > '$date_last'
            ORDER BY logobject.ActionTime ASC";

    $result = sqlsrv_query($con, $sql);

    while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {

        //$data_api[] = $row;

        switch ($row['system_type_object']) {

          case 'city':
          case 'building':
          case 'floor':
          case 'room':
          case 'rack group':
          case 'network equipment card':
          case 'network equipment gbic slot':
          case 'device in rack card':
          case 'device in rack gbic slot':
          case 'blade server card':
          case 'blade server gbic slot':
          case 'copper module':

            $type = str_replace('city', 'company', $row['system_type_object']);
            $type = str_replace('network equipment card', 'slot', $type);
            $type = str_replace('network equipment gbic slot', 'slot', $type);
            $type = str_replace('device in rack card', 'slot', $type);
            $type = str_replace('device in rack gbic slot', 'slot', $type);
            $type = str_replace('blade server card', 'slot', $type);
            $type = str_replace('blade server gbic slot', 'slot', $type);
            $type = str_replace('copper module', 'slot', $type);

            $path = explode("/", $row['path_object']);
            $id_parent = intval($path[count($path)-2]);

            $obj = array("id"=>$row['id_object'],
                         "name"=>$row['name_object'],
                         "description"=>$row['description_object'],
                         "type"=>$type,
                         "id_parent"=>$id_parent,
                         "action"=>$row['type_action'],
                         "date_log"=>$row['date_log']);

            if ($type == "slot") {
                $service = getServiceCardByID($con, $row['id_object']);
                $obj['service'] = $service;
            }

            $data_api[] = $obj;

            break;

          // Se for um blade server
          case 'blade server':
            $path = explode("/", $row['path_object']);
            $id_parent = intval($path[count($path)-2]); // ID do blade Enclosure
            $id_parent_parent = intval($path[count($path)-3]); // ID do rack

            if ($row['type_action'] == 2 || $row['type_action'] == 3) {
                $equipament = getEquipament($con, $row['id_object'], $row['system_type_object'], $capacity_fields);
                if ($equipament != null) {
                    $equipament['action'] = $row['type_action'];
                    $equipament['type'] = "device";
                    $equipament['date_log'] = $row['date_log'];
                    $equipament['id_parent'] = $id_parent_parent;
                    $equipament['id_equipament_parent'] = $id_parent;
                    $equipament['ru_top'] = null;
                    $equipament['ru_size'] = null;
                    $equipament['ru_buttom'] = null;
                    $data_api[] = $equipament;
                }

            } elseif ($row['type_action'] == 4) {
                $data_api[] = array("id"=>$row['id_object'],
                                  "type"=> "device",
                                  "id_parent"=>$id_parent_parent,
                                  "action"=>$row['type_action'],
                                  "date_log"=>$row['date_log']);
            }

            break;

          // Se for um rack
          case 'rack':

            $path = explode("/", $row['path_object']);
            $id_parent = intval($path[count($path)-2]);

            if ($row['type_action'] != 4) {

                $rack = getRackByID($con, $row['id_object'], $capacity_fields);
                $data_api[] = array("id"=>$rack['id_rack'],
                                    "name"=>$rack['name_rack'],
                                    "description"=>$rack['description_rack'],
                                    "zone"=>$rack['zone'],
                                    "position_zone"=>$rack['position_zone'],
                                    "total_ru"=>$rack['total_ru'],
                                    "created_date"=>$rack['created_date'],
                                    "potency_watts_limit"=>(($rack['potency_watts_limit'] == "0" || $rack['potency_watts_limit'] == null || $rack['potency_watts_limit'] == ".00") ? 0 : floatval($rack['potency_watts_limit'])),
                                    "electrical_sockets_limit"=>(($rack['electrical_sockets_limit'] == "0" || $rack['electrical_sockets_limit'] == null || $rack['electrical_sockets_limit'] == ".00") ? 0 : floatval($rack['electrical_sockets_limit'])),
                                    "type"=>$row['system_type_object'],
                                    "id_parent"=>$id_parent,
                                    "parent_type_name"=>$rack['parent_type_name'],
                                    "action"=>$row['type_action'],
                                    "date_log"=>$row['date_log']);
            } else {

                $data_api[] = array("id"=>$row['id_object'],
                                    "name"=>$row['name_object'],
                                    "description"=>$row['description_object'],
                                    "type"=>$row['system_type_object'],
                                    "id_parent"=>$id_parent,
                                    "action"=>$row['type_action'],
                                    "date_log"=>$row['date_log']);
            }

            break;

          default:

            $path = explode("/", $row['path_object']);
            $id_parent = intval($path[count($path)-2]);

            // Verifica se o componente é um equipamento. Para isso, verifica se o pai do componente é um rack
            if (isRack($con, $id_parent)) {

                if ($row['type_action'] == 2 || $row['type_action'] == 3) {

                    $equipament = getEquipament($con, $row['id_object'], $row['system_type_object'], $capacity_fields);
                    if ($equipament != null) {
                        $equipament['action'] = $row['type_action'];
                        $equipament['type'] = "device";
                        $equipament['date_log'] = $row['date_log'];
                        $data_api[] = $equipament;
                    }

                } elseif ($row['type_action'] == 4) {
                    $data_api[] = array("id"=>$row['id_object'],
                                      "type"=> "device",
                                      "id_parent"=>$id_parent,
                                      "action"=>$row['type_action'],
                                      "date_log"=>$row['date_log']);
                }


                break;
            }

            // Verifica se o componente é uma porta
            if ($row['table_system'] == "smendpoint") {

                // Porta atualizada
                if ($row['type_action'] == 3) {

                  $port = getPort($con, $row['id_object']);

                  if ($port['system_type_object_parent_parent'] == "rack" || $port['system_type_object_parent_parent'] == "blade enclosure") {
                      $data_api[] = array("id"=>$port['id'],
                                          "name"=>$port['name'],
                                          "description"=>$port['description'],
                                          "position"=>$port['position'],
                                          "velocity"=>$port['velocity'],
                                          "service"=>$port['service'],
                                          "type_port"=>$port['type'],
                                          "id_parent"=>$port['id_parent'],
                                          "id_slot_by_api"=>null,
                                          "slot_name"=>null,
                                          "slot_description"=>null,
                                          "status"=>$port['status'],
                                          "type"=>"port",
                                          "action"=>3,
                                          "date_log"=>$row['date_log']);

                  } else if ($port['system_type_object_parent_parent_parent'] == "rack" || $port['system_type_object_parent_parent_parent'] == "blade enclosure" ){
                      $data_api[] = array("id"=>$port['id'],
                                          "name"=>$port['name'],
                                          "description"=>$port['description'],
                                          "position"=>$port['position'],
                                          "velocity"=>$port['velocity'],
                                          "service"=>$port['service'],
                                          "type_port"=>$port['type'],
                                          "status"=>$port['status'],
                                          "id_parent"=>$port['id_parent_parent'],
                                          "id_slot_by_api"=>$port['id_parent'],
                                          "slot_name"=>$port['name_parent'],
                                          "slot_description"=>$port['description_parent'],
                                          "type"=>"port",
                                          "action"=>3,
                                          "date_log"=>$row['date_log']);

                  }

                // Remoção
                } elseif ($row['type_action'] == 4) {

                  $data_api[] = array("id"=>$row['id_object'],
                                      "type"=> "port",
                                      "action"=>$row['type_action'],
                                      "date_log"=>$row['date_log']);

                }

                break;
            }

            break;
        }
    }

    // Pega todas as portas criadas
    $ports_created = getPortsCreated($con, $date_last);
    foreach ($ports_created as $key => $port) {
        $data_api[] = $port;
    }

    // Pega todos os equipamentos modificados sem estar na tabela logs
    $equipaments_modified = getEquipaments($con, $date_last, $capacity_fields);
    foreach ($equipaments_modified as $key => $equipament) {
        $flag = false;
        foreach ($data_api as $key => $data) {
            if ($data['id'] == $equipament['id'] && new DateTime($data['date_log']) == new DateTime($equipament['date_log'])) {
                $flag = true;
                break;
            }
        }
        if (!$flag) {
            $data_api[] = $equipament;
        }
    }

    // Ordena a lista com os resultados
    $data_api = order($data_api);

    // Retorna o resultado
    echo json_encode($data_api);

} catch (Exception $ex) {
    $data = array("status"=>"failed");
    echo json_encode($data);

} finally {
    if ($con) {
        sqlsrv_free_stmt($result);
        sqlsrv_close($con);
    }

}

?>
